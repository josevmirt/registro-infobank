package com.infobank.registro.model;

public class Usuario {
	private int id;
	private String tipoPersona;
	private String numeroDoc;
	private String fechaEmiDoc;
	private String fechaVenDoc;
	private String nombre;
	private String email;
	private String telefonoPrin;
	private String telefonoSeg;
	
	public Usuario(int id, String tipoPersona, String numeroDoc, String fechaEmiDoc,  String fechaVenDoc, String nombre, String email, String telefonoPrin, String telefonoSeg) {
		this.setId(id);
		this.setTipoPersona(tipoPersona);
		this.setNumeroDoc(numeroDoc);
		this.setFechaEmiDoc(fechaEmiDoc);
		this.setFechaVenDoc(fechaVenDoc);
		this.setNombre(nombre);
		this.setEmail(email);
		this.setTelefonoPrin(telefonoPrin);
		this.setTelefonoSeg(telefonoSeg);


	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTipoPersona() {
		return tipoPersona;
	}

	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}

	public String getNumeroDoc() {
		return numeroDoc;
	}

	public void setNumeroDoc(String numeroDoc) {
		this.numeroDoc = numeroDoc;
	}

	public String getFechaEmiDoc() {
		return fechaEmiDoc;
	}

	public void setFechaEmiDoc(String fechaEmiDoc) {
		this.fechaEmiDoc = fechaEmiDoc;
	}

	public String getFechaVenDoc() {
		return fechaVenDoc;
	}

	public void setFechaVenDoc(String fechaVenDoc) {
		this.fechaVenDoc = fechaVenDoc;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefonoPrin() {
		return telefonoPrin;
	}

	public void setTelefonoPrin(String telefonoPrin) {
		this.telefonoPrin = telefonoPrin;
	}

	public String getTelefonoSeg() {
		return telefonoSeg;
	}

	public void setTelefonoSeg(String telefonoSeg) {
		this.telefonoSeg = telefonoSeg;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
