<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="./css/bootstrap.css" rel="stylesheet" type="text/css" />

    <title>Registror de personas Info Bank C.A</title>
</head>

<body>
    <h1>Banco Universal Info Bank C.A</h1>

    <div class="container">
        <a class="btn btn-primary btn-lg btn-block" href="adminUsuario?action=nuevo">Agregar Persona</a>
    </div>
    
    <br>
    
    <div class="container">
        <a class="btn btn-info btn-lg btn-block" href="adminUsuario?action=mostrar">Mostrar Personas</a>
    </div>


</body>

</html>