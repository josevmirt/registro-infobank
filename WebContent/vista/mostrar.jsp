<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
  <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>

<title>Registror de personas Info Bank C.A</title>
</head>
<body>
	<h1>Lista  Personas</h1>
	<table>
		<tr>
			<td><a class="btn btn-success" href="adminUsuario?action=index" >Ir al men�</a> </td>
		</tr>
	</table>
	<br>
	<table class="table table-bordered" width="100%">
		<tr>
		 <td class="text-center">ID</td>
		 <td class="text-center">Tipo de Persona N/J</td>
		 <td class="text-center">Numero de Documento</td>
		 <td class="text-center">Fecha de emisi�n del documento</td>
		 <td class="text-center">Fecha de vencimiento del documento</td>
		 <td class="text-center">Nombre</td>
		 <td class="text-center">Correo</td>
		 <td class="text-center">Tel�fono principal</td>
		 <td class="text-center">Tel�fono secundario</td>


		 <td colspan=2>ACCIONES</td>
		</tr>
		<c:forEach var="usuario" items="${lista}">
			<tr>
				<td><c:out value="${usuario.id}"/></td>
				<td><c:out value="${usuario.tipoPersona}"/></td>
				<td><c:out value="${usuario.numeroDoc}"/></td>
				<td><c:out value="${usuario.fechaEmiDoc}"/></td>
				<td><c:out value="${usuario.fechaVenDoc}"/></td>
				<td><c:out value="${usuario.nombre}"/></td>
				<td><c:out value="${usuario.email}"/></td>
				<td><c:out value="${usuario.telefonoPrin}"/></td>
				<td><c:out value="${usuario.telefonoSeg}"/></td>
				<td><a  class="btn btn-warning" href="adminUsuario?action=showedit&id=<c:out value="${usuario.id}" />">Editar</a></td>
				<td><a  class="btn btn-danger" href="adminUsuario?action=eliminar&id=<c:out value="${usuario.id}"/>">Eliminar</a> </td>				
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>