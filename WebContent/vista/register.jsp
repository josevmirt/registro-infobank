<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
    <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />

        <title>Registror de personas Info Bank C.A</title>
    </head>

    <body>
        <div class="container">

            <div class="col-lg-6">
                <h1>Registrar Persona</h1>
                <form action="adminUsuario?action=register" method="post">

                    <label for="nombre">Nombre</label>
                    <input class="form-control" type="text" name="nombre" required>

                    <label for="opciones">Tipo de persona :</label>
                    <select class="form-control" name="tipoPersona" required>
                        <option value="" disabled selected>- Seleccione -</option>
                        <option value="Natural">Natural</option>
                        <option value="Juridica">Juridica</option>
                    </select>

                    <label for="presupuesto">Numero de Documento:</label>
                    <input class="form-control" type="number" name="numeroDoc" required>

                    <label for="fecha">Fecha de emisi�n del documento:</label>
                    <input class="form-control" type="date" name="fechaEmiDoc" required>

                    <label for="fecha">Fecha de vencimiento del documento:</label>
                    <input class="form-control" type="date" name="fechaVenDoc" required>

                    <label for="email">E-mail</label>
                    <input class="form-control" type="email" name="email" required>

                    <label for="telefono">Tel�fono Pricipal</label>
                    <input class="form-control" type="number" name="telefonoPrin" required>

                    <label for="telefono">Tel�fono Secundario</label>
                    <input class="form-control" type="number" name="telefonoSeg">

                    <input type="submit" class="btn btn-primary" value="Agregar" name="agregar">
                </form>

            </div>
        </div>


    </body>

    </html>