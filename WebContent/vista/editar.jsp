<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" />
    
    <title>Registror de personas Info Bank C.A</title>
</head>

<body>
    <div class="container">

        <div class="col-lg-6">
            <h1>Actualizar Personas</h1>
            <form action="adminUsuario?action=editar" method="post">


                <label for="id">ID</label>
                <input class="form-control" type="text" name="id" value="<c:out value="${usuario.id}"></c:out>"
                required>

                <label for="nombre">Nombre</label>
                <input class="form-control" type="text" name="nombre" value="<c:out value="${usuario.nombre}"></c:out>"
                required>

                <label for="opciones">Tipo de persona :</label>
                <select class="form-control" name="tipoPersona" required>
                    <option value="" disabled selected>- Seleccione -</option>
                    <option value="Natural">Natural</option>
                    <option value="Juridica">Juridica</option>
                </select>

                <label for="presupuesto">Numero de Documento:</label>
                <input class="form-control" type="number" name="numeroDoc" value="<c:out value="${usuario.numeroDoc}">
                </c:out>"
                required>

                <label for="fecha">Fecha de emisi�n del documento:</label>
                <input class="form-control" type="date" name="fechaEmiDoc" value="<c:out value="${usuario.fechaEmiDoc}">
                </c:out>" required>

                <label for="fecha">Fecha de vencimiento del documento:</label>
                <input class="form-control" type="date" name="fechaVenDoc" value="<c:out value="${usuario.fechaVenDoc}">
                </c:out>" required>

                <label for="email">E-mail</label>
                <input class="form-control" type="email" name="email" value="<c:out value="${usuario.email}"></c:out>"
                required>

                <label for="telefono">Tel�fono Pricipal</label>
                <input class="form-control" type="number" name="telefonoPrin" value="<c:out value="${usuario.telefonoPrin}">
                </c:out>" required>

                <label for="telefono">Tel�fono Secundario</label>
                <input class="form-control" type="number" name="telefonoSeg" value="<c:out value="${usuario.telefonoSeg}">
                </c:out>">



                <input type="submit" name="registrar" value="Guardar">
            </form>
        </div>
    </div>

</body>

</html>